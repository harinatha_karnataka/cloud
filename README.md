

Zuul �  gateway service that provides dynamic routing, monitoring, resiliency, security, and more
Eureka � service registration and discovery


Project Details :-
------------------------
1. eureka_service :- 
----------------------
Add the following annotations on the main class 
@EnableZuulProxy
@EnableEurekaServer

application.yml:-
-----------------
	spring:
	  application:
		name: eureka-service
	server:
		port : 8302
	eureka:
	  client:
		registerWithEureka: false
		fetchRegistry: false
		server:
		  waitTimeInMsWhenSyncEmpty: 0

	zuul:
	  prefix: /api
	  routes:
		employee-service:
		  path: /employee-service/**
		  url: http://localhost:8090
	  
pom.xml:-
----------
Add the following dependencies 

		<dependency>
			<groupId>org.springframework.cloud</groupId>
			<artifactId>spring-cloud-starter-zuul</artifactId>
		<version>1.3.0.RELEASE</version>
		</dependency>
		<dependency>
			<groupId>org.springframework.cloud</groupId>
			<artifactId>spring-cloud-starter-eureka-server</artifactId>
		</dependency>
		
example :- 
-------------
	http://localhost:8302/api/employee-service/employee  -- use this to access employee service data using gateway .


Client :- 
--------------

pom.xml :
----------
	<dependency>
			<groupId>org.springframework.cloud</groupId>
			<artifactId>spring-cloud-starter-eureka-server</artifactId>
			<version>1.3.0.RELEASE</version>
		</dependency>
		
application.yml 
---------------
	spring:
	  application:
		name: employee-service
	  server:
		port : 8090
	eureka:
	  client:
		registerWithEureka: true
		fetchRegistry: true
		serviceUrl:
		  defaultZone: http://localhost:8302/eureka/
	  instance:
		hostname: localhost
	
application.properties :-  for health / acurator services 
----------------------------

	server.port:8090
	endpoints.health.id=health
	endpoints.health.sensitive=false
	management.security.enabled=false

main class :-  add the following annotations 
---------------
	@EnableDiscoveryClient
	@EnableEurekaClient  

Examples :- 
	http://localhost:8090/employee  -- access using direct cleint 
	http://localhost:8090/actuator  -- acurator 

custom health creation :-
---------------------
	@Component
	public class CustomHealth implements HealthIndicator {

		@Override
		public Health health() {

			Builder builder = Health.up();
			builder.withDetail("Key", "value");
			return builder.build();
		}

	}

custom endpoint creation for acurator :-
-------------------------------------
	@Component
	public class MyCustomEndpoint implements Endpoint<List<String>>{

		 @Override
		 public String getId() {
		  return "myCustomEndpoint";
		 }

		 @Override
		 public List<String> invoke() {
		  // Custom logic to build the output
				List<String> list = new ArrayList<String>();
				list.add("App message 1");
				list.add("App message 2");
				list.add("App message 3");
				list.add("App message 4");
				return list;
		 }

		 @Override
		 public boolean isEnabled() {
		  return true;
		 }

		 @Override
		 public boolean isSensitive() {
		  return true;
		 }

		}

Sample uri :- 
-----------------
	http://localhost:8090/myCustomEndpoint












